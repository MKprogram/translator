# To jest miejsce od którego należy zacząć zadanie TDD

#-------------First---------------------
#Szyfr 
#Kod
class Code_Cezar_this(object):

    def chang(self, txt, key):
        if txt == None or key == 0 or type(key) == str:
		#show_results
            return None
        #another
        else:
            txtchang = ""
            for n in range(len(txt)):
                if ord(txt[n]) > 122 - key:
                    txtchang += chr(ord(txt[n]) + key - 5)
                else:
                    txtchang += chr(ord(txt[n]) + key)
            return txtchang
            
#----------------------Next--------------------------------------------
#For_TDD

import unittest
from tests.Code_Cezar_this import Code_Cezar_this

class TestExist(unittest.TestCase):
    def testExistClassCode_Cezar_this (self):
        var = Code_Cezar_thisClass()

class TestCode_Cezar_this (unittest.TestCase):
    var = Code_Cezar_thisClass()
    def testNullTxt(self):
        self.assertEqual(None, self.var.chang(None,3))

class TestCode_Cezar_this2(unittest.TestCase):
    var = Code_Cezar_thisClass()
    def testTxt(self):
        self.assertEqual("poko",self.var.chang("spok",3))

class TestCode_Cezar_this 3(unittest.TestCase):
    var = Code_Cezar_thisClass()
    def testNullkey(self):
        self.assertEqual(None, self.var.chang("spok",0))

class TestCode_Cezar_this4(unittest.TestCase):
    var = Code_Cezar_thisClass()
    def teststringkey(self):
        self.assertEqual(None, self.var.chang("spok","a"))

